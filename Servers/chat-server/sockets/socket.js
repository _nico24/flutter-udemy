const { comprobarJWT } = require('../helpers/jwt');
const { io } = require('../index');
const {usuarioConectado, usuarioDesconectado} = require ("../controllers/sockets")

// Mensajes de Sockets
io.on('connection', client => {
    console.log('Cliente conectado');

    const [valido,uid] = comprobarJWT(client.handshake.headers["x-token"])

    //! Verificar autentificacion
    if (!valido ){return client.disconnect();}

    //! Cliente autenticado
    usuarioConectado(uid);

    //! Cliente Desconectado
    client.on('disconnect', () => {
        usuarioDesconectado(uid);
    });

});

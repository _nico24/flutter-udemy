import 'dart:async';

import 'package:estados_app/models/usuario.dart';
import 'package:flutter/material.dart';

class _UsuarioService with ChangeNotifier {
  late Usuario _usuario;

  final StreamController<Usuario> _usuarioStreamController =
      StreamController<Usuario>.broadcast();

  Usuario? get usuario => _usuario;
  // ignore: unnecessary_null_comparison
  bool get existeUsuario => (_usuario != null) ? true : false;

  Stream<Usuario> get usuarioStream => _usuarioStreamController.stream;

  void cargarUsuario(Usuario user) {
    _usuario = user;
    _usuarioStreamController.add(user);
  }

  void cambiarEdad(int edad) {
    _usuario.edad = edad;
    _usuarioStreamController.add(_usuario);
  }

  @override
  dispose() {
    super.dispose();
    _usuarioStreamController.close();
  }
}

final usuarioService = _UsuarioService();

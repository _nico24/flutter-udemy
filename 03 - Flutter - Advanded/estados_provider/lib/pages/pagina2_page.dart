// ignore_for_file: depend_on_referenced_packages

import 'package:estados_provider/services/usuario_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/usuario.dart';

class Pagina2Page extends StatelessWidget {
  const Pagina2Page({super.key});

  @override
  Widget build(BuildContext context) {
    final usuarioService = Provider.of<UsuarioService>(context);

    return Scaffold(
      appBar: AppBar(
        title: usuarioService.existeUsuario
            ? Text("Nombre: ${usuarioService.usuario?.nombre}")
            : const Text("Pagina2"),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MaterialButton(
              color: Colors.blue,
              onPressed: () {
                final newUser = Usuario(
                    nombre: "Nicolas Alexis",
                    edad: 24,
                    profesiones: [
                      "Fullstack Developer",
                      "Video Jugador Experto"
                    ]);
                usuarioService.usuario = newUser;
              },
              child: const Text('Establecer Usuario',
                  style: TextStyle(color: Colors.white))),
          MaterialButton(
              color: Colors.blue,
              onPressed: () {
                usuarioService.cambiarEdad(27);
              },
              child: const Text('Cambiar Edad',
                  style: TextStyle(color: Colors.white))),
          MaterialButton(
              color: Colors.blue,
              onPressed: () {
                usuarioService.agregarProfesion();
              },
              child: const Text('Añadir Profesion',
                  style: TextStyle(color: Colors.white))),
        ],
      )),
    );
  }
}

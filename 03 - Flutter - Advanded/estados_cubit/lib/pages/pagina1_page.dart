import 'package:estados_cubit/bloc/usuario/usuario_cubit.dart';
import 'package:estados_cubit/models/usuario.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import "package:flutter_bloc/flutter_bloc.dart";

class Pagina1Page extends StatelessWidget {
  const Pagina1Page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagina 1'),
        actions: [
          IconButton(
              onPressed: () => context.read<UsuarioCubit>().borrarUsuario(),
              icon: const Icon(Icons.exit_to_app))
        ],
      ),
      body: const BodyScaffold(),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.accessibility_new),
          onPressed: () => Navigator.pushNamed(context, 'pagina2')),
    );
  }
}

class BodyScaffold extends StatelessWidget {
  const BodyScaffold({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UsuarioCubit, UsuarioState>(builder: (_, state) {
      switch (state.runtimeType) {
        case UsuarioInitial:
          return const Center(child: Text("No hay informacion del usuario"));
          // ignore: dead_code
          break;

        case UsuarioActivo:
          return InformacionUsuario((state as UsuarioActivo).usuario);
          // ignore: dead_code
          break;

        default:
          return const Center(child: Text("Estado desconocido"));
      }

//      if (state is UsuarioInitial) {
//        return Center(child: Text("No hay informacion del usuario"));
//      } else if (state is UsuarioActivo) {
//        return InformacionUsuario(state.usuario);
//      } else {
//         Agregar un retorno adicional en caso de que ninguna condición se cumpla
//        return Center(child: Text("Estado desconocido"));
//      }
    });
  }
}

class InformacionUsuario extends StatelessWidget {
  final Usuario usuario;
  const InformacionUsuario(this.usuario, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('General',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          const Divider(),
          ListTile(title: Text('Nombre: ${usuario.nombre}')),
          ListTile(title: Text('Edad: ${usuario.edad}')),
          const Text('Profesiones',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          const Divider(),
          ...usuario.profesiones.map((profesion) => ListTile(
                title: Text(profesion),
              ))
        ],
      ),
    );
  }
}

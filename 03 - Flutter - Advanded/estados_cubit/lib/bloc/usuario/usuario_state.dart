part of "usuario_cubit.dart";

@immutable
abstract class UsuarioState {}

class UsuarioInitial extends UsuarioState {
  final existeUsuario = false;
}

class UsuarioActivo extends UsuarioState {
  final existeUsuario = true;
  final Usuario usuario;

  UsuarioActivo(this.usuario);
}

//! context.bloc()
//! ya no es válida desde la versión 7 de Flutter_bloc, entonces debes de escribir esto:
//! context.read()
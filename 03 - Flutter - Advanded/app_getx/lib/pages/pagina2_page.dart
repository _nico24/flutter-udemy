// ignore_for_file: depend_on_referenced_packages

import 'package:app_getx/controllers/usuario_controller.dart';
import 'package:app_getx/models/usuario.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Pagina2Page extends StatelessWidget {
  const Pagina2Page({super.key});

  @override
  Widget build(BuildContext context) {
    // print(Get.arguments);
    final usuarioCtrl = Get.find<UsuarioController>();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagina 2'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MaterialButton(
              color: Colors.blue,
              onPressed: () {
                usuarioCtrl.cargarUsuario(Usuario(nombre: "Nicolas", edad: 25));
                Get.snackbar(
                    "Usuario establecido", "Nicolas es el nombre del usuario",
                    backgroundColor: Colors.white,
                    boxShadows: [
                      const BoxShadow(color: Colors.black38, blurRadius: 10)
                    ],
                    duration: const Duration(milliseconds: 1000));
              },
              child: const Text('Establecer Usuario',
                  style: TextStyle(color: Colors.white))),
          MaterialButton(
              color: Colors.blue,
              onPressed: () {
                usuarioCtrl.cambiarEdad(35);
              },
              child: const Text('Cambiar Edad',
                  style: TextStyle(color: Colors.white))),
          MaterialButton(
              color: Colors.blue,
              onPressed: () {
                usuarioCtrl.agregarProfesion(
                    "Profesion #${usuarioCtrl.profesionesCount + 1}");
              },
              child: const Text('Añadir Profesion',
                  style: TextStyle(color: Colors.white))),
          MaterialButton(
              color: Colors.blue,
              onPressed: () {
                Get.changeTheme(
                    Get.isDarkMode ? ThemeData.light() : ThemeData.dark());
              },
              child: const Text('Cambiar Tema',
                  style: TextStyle(color: Colors.white))),
        ],
      )),
    );
  }
}

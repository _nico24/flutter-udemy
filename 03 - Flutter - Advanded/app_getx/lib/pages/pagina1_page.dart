// ignore_for_file: unused_import, depend_on_referenced_packages

import 'package:app_getx/controllers/usuario_controller.dart';
import 'package:app_getx/pages/pagina2_page.dart';
import 'package:flutter/material.dart';
import "package:get/get.dart";

import '../models/usuario.dart';

class Pagina1Page extends StatelessWidget {
  const Pagina1Page({super.key});

  @override
  Widget build(BuildContext context) {
    final usuarioCtrl = Get.put(UsuarioController());

    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagina 1'),
      ),
      body: Obx(() => usuarioCtrl.existeUsuario.value
          ? InformacionUsuario(
              usuario: usuarioCtrl.usuario.value,
            )
          : const NoInfo()),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.accessibility_new),
          //  onPressed: () => Navigator.pushNamed(context, 'pagina2')
          onPressed: () => Get.toNamed("pagina2", arguments: {
                "nombre": "Nicolas",
                "edad": 25,
              })),
    );
  }
}

class NoInfo extends StatelessWidget {
  const NoInfo({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text("No hay usuario seleccionado"),
    );
  }
}

class InformacionUsuario extends StatelessWidget {
  final Usuario usuario;

  const InformacionUsuario({super.key, required this.usuario});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('General',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          const Divider(),
          ListTile(title: Text('Nombre: ${usuario.nombre}')),
          ListTile(title: Text('Edad: ${usuario.edad}')),
          const Text('Profesiones',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          const Divider(),

          ...usuario.profesiones.map((profesion) => ListTile(
                title: Text(profesion),
              ))
          //      ListTile(title: Text('Profesion 1')),
          //      ListTile(title: Text('Profesion 1')),
          //      ListTile(title: Text('Profesion 1')),
        ],
      ),
    );
  }
}

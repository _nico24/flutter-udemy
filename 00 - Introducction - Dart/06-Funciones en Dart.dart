void main() {
  //!Creando la variable "nombre" = "Fernando"
  final nombre = 'Fernando';

//!   saludar( nombre, 'Greetings' );
  saludar2(nombre: nombre, mensaje: 'Greetings');
}

void saludar(String nombre, [String mensaje = 'Hi']) {
  print('$mensaje $nombre');
}

//!"Required" siempre que alguien va usar la funcion
//!va a tener que enviar el nombre y el mensaje
void saludar2({
  required String nombre,
  required String mensaje,
}) {
  print('$mensaje $nombre');
}

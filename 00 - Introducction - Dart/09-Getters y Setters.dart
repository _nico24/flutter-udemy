//Importa todo el paquete de dart
import 'dart:math' as math;

void main() {
  final cuadrado = new Cuadrado(2);

  cuadrado.area = 100;

  print('area: ${cuadrado.calculaArea()}');

  print('lado: ${cuadrado.lado}');
  print('area get: ${cuadrado.area}');
}

//!Creando una nueva clase, metodo especial
class Cuadrado {
  double lado; //! lado * lado

  //!Getter
  double get area {
    return this.lado * this.lado;
  }

  //!Setter
  set area(double valor) {
    //!"mathsqrt" es un valor importado de "dart"
    this.lado = math.sqrt(valor);
  }

  Cuadrado(double lado) : this.lado = lado;

  double calculaArea() {
    return this.lado * this.lado;
  }
}

void main() {
  //!Creando una lista de numeros enteros TEST PUSH
  List<int> numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  //!Agregando el numero 11 a la lista "numeros"
  numeros.add(11);

  //!Imprimiendo la variable numeros
  print(numeros);

  //! Generando una Lista de 100 numeros comenzando desde el "0"
  //! Hasta el "99"
  final masNumeros = List.generate(100, (int index) => index);

  //!Imprimiendo la variable "masNumeros"
  print(masNumeros);
}

int? encontrarNumeroMasFrecuente(List<int> numeros) {
  //! Creo un mapa para contar la frecuencia de cada número
  Map<int, int> frecuencia = {};

  for (int numero in numeros) {
    //! Si el numero no está en el mapa se inicializa su frecuencia en 0 y luego se incrementa en 1
    frecuencia[numero] = (frecuencia[numero] ?? 0) + 1;
  }

  //! Encontrar el número con la frecuencia más alta
  int? numeroMasFrecuente;
  int frecuenciaMasAlta = 0;

  frecuencia.forEach((numero, frecuencia) {
    //! Si se encuentra una frecuencia más alta, se actualiza el numero más frecuente y la frecuencia mas alta
    if (frecuencia > frecuenciaMasAlta) {
      frecuenciaMasAlta = frecuencia;
      numeroMasFrecuente = numero;
    }
  });
  //! Estoy retornando numeroMasFrecuente
  return numeroMasFrecuente;
}

void main() {
  List<int> lista = [1, 2, 3, 4, 2, 2, 3, 4, 4, 4]; //! Lista de numeros

  //! Llamamos a  encontrarNumeroMasFrecuente y guardamos el resultado en una variable.
  int? numeroMasFrecuente = encontrarNumeroMasFrecuente(lista);

  //! Imprimimos el número más frecuente de lo contrario hara"null".
  print('El numero más frecuente es: $numeroMasFrecuente');
}

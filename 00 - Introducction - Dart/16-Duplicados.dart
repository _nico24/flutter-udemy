//! Elimino duplicados en una lista
List<T> eliminarpalabras<T>(List<T> lista) {
  return lista.toSet().toList();
}

void main() {
  //! Lista con duplicados
  List<String> palabras = ['Hola', 'Mundo', 'Hola', 'Programacion', 'Mundo'];

  //! Llamo a "eliminarpalabras"
  List<String> palabrasSinDuplicar = eliminarpalabras(palabras);

  //!Imprimo mi resultado
  print(palabrasSinDuplicar);
}

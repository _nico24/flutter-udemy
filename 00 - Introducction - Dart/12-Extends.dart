void main() {
  final superman = new Heroe("Clark Kent");
  final luthor = new Villano("Lex Luxthor");

  print(superman);
  print(luthor);
}

//! creando clase abstracta
abstract class Personaje {
  //!Poder opcional
  String? poder;
  String nombre;

  Personaje(this.nombre);

  @override
  String toString() {
    return "$nombre - $poder";
  }
}

class Heroe extends Personaje {
//!Consultor
//!"super" llamado al constructor de la clase "super"
  Heroe(String nombre) : super(nombre);
}

class Villano extends Personaje {
  int maldad = 50;
//!super llamado del constructor para hacer funcionar a "Villano"
  Villano(String nombre) : super(nombre);
}

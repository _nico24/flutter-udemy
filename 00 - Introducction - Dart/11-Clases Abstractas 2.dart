void main() {
  //!Creando objeto Perro y Gato
  final perro = new Perro();
  final gato = new Gato();

  sonidoAnimal(perro);
  sonidoAnimal(gato);
}

void sonidoAnimal(Animal animal) {
  animal.emitirSonido();
}

//!Una clase asbtracta es una clase que no se puede instanciar"
abstract class Animal {
  //!cantidad de patas "opcional"
  int? patas;
  void emitirSonido();
}

//!creando otra clase abstracta perro
class Perro implements Animal {
  //!cantidad de patas? = "Opcional"
  int? patas;

  void emitirSonido() => print('Guauuuuuuuu');
}

//!Creando clase abstracta gato
class Gato implements Animal {
  //!Cantidad de patas? = opcional
  //!Cantidad de cola? = opcinal
  int? patas;
  int? cola;

  void emitirSonido() => print('Miauuuuuuuu');
}

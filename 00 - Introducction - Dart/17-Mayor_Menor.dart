void main() {
  //! Establesco una lista de numero aleatorios
  List<int> numeros = [5, 2, 9, 1, 7];

  //! estoy llamando a la funcion ordenarLista
  ordenarLista(numeros);

  //!imprimo el resultado
  print("Lista de mayor a menor");
  print(numeros);
}

//! Creo la funcion ordenarLista
void ordenarLista(List<int> lista) {
  //! sort = ordenar lista
  lista.sort(

      //!determina el orden de los elementos
      (a, b) => b.compareTo(a));
}
